$fn=100;

arcZ = 130/2.;
arcR = 3/2.;
douilleR =  18.6;
recX = 2;
recY = 4;
layerZ = 0.25;
baseGap= arcR+2;
topRingX = 2;
topRingY = 2.15;
topRingZ = 61.2;
twistyZ = 60;
topRingTwistyZ = 60 - twistyZ + 2.5;

module arc(){
  rotate([90,0,0])
    translate([0,0,-arcZ])
      rotate_extrude(angle=180)
        translate([arcZ,arcZ,0])
          //circle(arcR);
          square([recX,recY],center=true);
}

module arcs(nbArcs){
  echo(360/nbArcs);
  for(i=[0: 360/nbArcs :180])
    rotate([0,0,i])
      arc();
}
module ring(rect=true,ang=360){
  //translate([0,0,-arcZ])
  rotate([0,0,-ang/2.])
  rotate_extrude(angle=ang)
    translate([arcZ+recX/4.,0,0])
      if (rect){
        square([recX,recY],center = true);
      }
      else{
        circle(arcR);
      }
}

module base(){
  translate([0,0,-recY/2])
  difference(){
    union(){
      cylinder(h=layerZ,r=arcZ);
      cylinder(h=layerZ*8,r=arcZ-arcR-baseGap);
    }
    cylinder(h=10*layerZ,r=arcZ-baseGap-20);
  }
}

module arcsCut(nb=1){
  difference(){
    arcs(nb);
    cylinder(h=arcZ*2,r=douilleR);
  }
}
module arcPairSlice(){
  ringAngle = 360;
  arcsCut();
  translate([0,0,recY/2.]){
    mirror([1,0,0]){
      ring(true,ringAngle);
      baseSlice();
    }
    ring(true,ringAngle);
    baseSlice();
  }
}

  
module all() {
  arcPairSlice();
  rotate([0,0,90])
    arcPairSlice();
  topRing();
}
module pieSlice(a, r, h){
  // a:angle, r:radius, h:height
  rotate_extrude(angle=a,$fn=100) square([r,h]);
}
//pieSlice(15,arcZ,3);

module baseSlice(an=15){
  translate([0,0,-recY/2])
  rotate([0,0,-an/2.])
  difference(){
    union(){
      pieSlice(an,arcZ,layerZ);
      pieSlice(an,,arcZ-arcR-baseGap,h=layerZ*8);
    }
    cylinder(h=10*layerZ,r=arcZ-baseGap-40);
  }
}

module topRing(){
  translate([0,0,topRingZ])
    rotate_extrude()
      translate([douilleR,0,0])
        square([topRingX,topRingY]);
}
//intersection(){
//all();
module twisty(){
  //fudgeFactor = 1.05; // twistyZ == 60
  fudgeFactor = 1.05;    // twistyZ == 40
  rFactor =  1;
  linear_extrude(twistyZ,twist=180,scale=[fudgeFactor*douilleR/arcZ,fudgeFactor*douilleR/arcZ])
  
    translate([arcZ,0,0])
      //circle(arcR);
      square([rFactor*recX,rFactor*recY],center=true);
}
module t4(){
//projection(){
twisty();
rotate([0,0,90])
  twisty();
rotate([0,0,180])
  twisty();
rotate([0,0,270])
  twisty();
ring();
translate([0,0,-topRingTwistyZ])
topRing();
//}
}
t4();
module twistyNoScale(){
  //fudgeFactor = 1.05; // twistyZ == 60
  fudgeFactor = 1.05;    // twistyZ == 40
  rFactor =  1;
  linear_extrude(twistyZ,twist=180)
    translate([arcZ,0,0])
      //circle(arcR);
      square([rFactor*recX,rFactor*recY],center=true);
}
/*minkowski(){
twistyNoScale();
cylinder(twistyZ,arcZ,douilleR);
}
*/