$fn=250;

DXFFileName = "DXF/douille profile_09_lite v5.dxf";
layerName   = "3";
douilleZ    = 90;


module douilleIt(){
rotate_extrude()
  import(DXFFileName, layer=layerName);
}
douilleIt();

module timeKiller(){
  nozzleD = 0.5;
  nbPerimeters = 3;
  oR = 10;
  iR = oR-(nbPerimeters*nozzleD);
  baseZ = 3;
  difference(){
    cylinder(h=douilleZ,r=oR);
    cylinder(h=douilleZ,r=iR);
  }
  cylinder(h=baseZ,r=oR+10);
}
//timeKiller();

include <threads.scad>;

module temp(){
  metric_thread(diameter=14, pitch=2.82, length=14);
  translate([0,0,-10])
    cylinder(h=10,r=5.373);
}
bulbBaseZ = 5;
baseLipZ = 0.5;
baseColZ = 7;
threadBaseZ = baseLipZ + baseColZ ; 
threadZ = 14;
topConeZ =  3.5;

module bulbBase(){
  cylinder(h=bulbBaseZ,r1=12,r2=9);
}
//bulbBase();

module threadBase(){
  lipH = baseLipZ;
  lipR = 18/2.;
  colH = baseColZ;
  colR = 17/2.;
  cylinder(h=lipH,r=lipR);
  translate([0,0,lipH])
    cylinder(h=colH,r=colR);  
}
//threadBase();

module threadedPart(){
  metric_thread(diameter=14, pitch=2.82, length=threadZ);
}
//threadedPart();

module topCone(){
  rBase = 10/2.;
  rTop  =  5/2.;
  Ht    = topConeZ;
  cylinder(h=Ht,r1=rBase,r2=rTop);
}
//topCone();
module buildAll(){
  bulbBase();
  translate([0,0,bulbBaseZ])
    threadBase();
  translate([0,0,bulbBaseZ+threadBaseZ])
    threadedPart();
  translate([0,0,bulbBaseZ+threadBaseZ+threadZ])
    topCone();
}
//buildAll();